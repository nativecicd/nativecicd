import React, {useCallback, useRef, useState} from 'react';
import ReactFlow, {
  addEdge,
  Controls,
  ReactFlowInstance,
  ReactFlowProvider,
  useEdgesState,
  useNodesState
} from 'reactflow';

import 'reactflow/dist/style.css';
import {ResizableHandle, ResizablePanel, ResizablePanelGroup} from "../components/ui/resizable";
import {ActionList} from "./ActionList";

const initialNodes = [
  {id: '1', position: {x: 0, y: 0}, data: {label: '1'}},
  {id: '2', position: {x: 0, y: 100}, data: {label: '2'}},
  {id: '3', position: {x: 0, y: 200}, data: {label: '3'}},
];
const initialEdges = [{id: 'e1-2', source: '1', target: '2'}];

let id = 0;
const getId = () => `action_node_${id++}`;

export default function MainBuilder() {
  const reactFlowWrapper = useRef(null);
  const [reactFlowInstance, setReactFlowInstance] = useState<ReactFlowInstance | null>(null);

  const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);

  const onConnect = useCallback(
    (params: any) => setEdges((eds) => addEdge(params, eds)),
    [setEdges],
  );

  const onDragOver = useCallback((event: any) => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }, []);

  const onDrop = useCallback(
    (event: any) => {
      event.preventDefault();

      const type = event.dataTransfer.getData('application/reactflow');

      // check if the dropped element is valid
      if (typeof type === 'undefined' || !type) {
        return;
      }

      if (!reactFlowInstance) return;
      const position = reactFlowInstance.screenToFlowPosition({
        x: event.clientX,
        y: event.clientY,
      });
      const newNode = {
        id: getId(),
        type,
        position,
        data: {label: `${type} node`},
      };

      setNodes((nds) => nds.concat(newNode));
    },
    [reactFlowInstance, setNodes],
  );

  return (
    <div className={"flex flex-row h-screen"}>
      <ReactFlowProvider>
        <ResizablePanelGroup direction="horizontal">
          <ResizablePanel defaultSize={20}>
            <ActionList/>
          </ResizablePanel>
          <ResizableHandle withHandle/>
          <ResizablePanel defaultSize={60}>
            <div className={"w-full h-full"} ref={reactFlowWrapper}>
              <ReactFlow
                nodes={nodes}
                edges={edges}
                onNodesChange={onNodesChange}
                onEdgesChange={onEdgesChange}
                onConnect={onConnect}
                onInit={setReactFlowInstance}
                onDrop={onDrop}
                onDragOver={onDragOver}
                fitView
              >
                <Controls/>
              </ReactFlow>
            </div>
          </ResizablePanel>
          <ResizableHandle withHandle/>
          <ResizablePanel defaultSize={20}>Right</ResizablePanel>
        </ResizablePanelGroup>
      </ReactFlowProvider>
    </div>
  )
    ;
}