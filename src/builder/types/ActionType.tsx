import React from "react";
import {Handle, Position} from "reactflow";

export function handleActionNodeClick(nodeKey: string) {
  console.log("Node : " + nodeKey + " Clicked")
}

export type ActionNodeType = {
  nodeType: "workflow" | "stages" | "variables" | "UNKNOWN",
  allowedParent: string[]
  name: string,
  receptor?: object
  parameters?: object,
}

export const ActionNodeMapper = (node: any) => {
  return {
    id: node.id,
    type: node.type,
    position: node.position,
    data: node.data,
  }
}

const ActionNode = ({...data}) => {
  let receptorLeft: React.JSX.Element = <></>
  let receptorTop: React.JSX.Element = <></>
  let receptorRight: React.JSX.Element = <></>
  let receptorBottom: React.JSX.Element = <></>

  if (data.data.receptor.left) {
    receptorLeft = (
      <Handle id={data.data.receptor.left} type="source" position={Position.Left}
              className={`h-6 rounded-md bg-green-300`}/>)
  }
  if (data.data.receptor.top) {
    receptorTop = (
      <Handle id={data.data.receptor.top} type="target" position={Position.Top}
              className={`w-12 rounded-md bg-stone-900`}/>)
  }
  if (data.data.receptor.right) {
    receptorRight = (
      <Handle id={data.data.receptor.right} type="source" position={Position.Right}
              className={`h-6 rounded-md bg-red-300`}/>)
  }
  if (data.data.receptor.bottom) {
    receptorBottom = (
      <Handle id={data.data.receptor.bottom} type="source" position={Position.Bottom}
              className={`w-12 rounded-md bg-stone-900`}/>)
  }
  return (
    <div className={`shadow-md rounded-md border-2 bg-stone-900`}>
      <div className="flex items-center" onClick={() => handleActionNodeClick(data.data.key)}>
        <div
          className={`p-2 text-sm font-semibold text-stone-100`}>{data.data.name}</div>
        {receptorLeft}
        {receptorTop}
        {receptorRight}
        {receptorBottom}
      </div>
    </div>
  )
}