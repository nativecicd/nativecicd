export const getAvailableNodes = () => {
  return [
    {
      name: "Workflow",
      nodeType: "workflow",
      allowedParent: [],
      parameters: {},
      receptor: {}
    },
    {
      name: "Stages",
      nodeType: "stages",
      allowedParent: ["workflow"],
      parameters: {},
      receptor: {}
    },
    {
      name: "Variables",
      nodeType: "variables",
      allowedParent: ["workflow"],
      parameters: {},
      receptor: {}
    }
  ]

}
