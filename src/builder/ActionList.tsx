import React, {useState} from "react";
import {ActionNodeType} from "@/builder/types/ActionType";
import {getAvailableNodes} from "./data/ActionRepository";

export const ActionList = () => {
  const onDragStart = (event: any, node: ActionNodeType) => {
    event.dataTransfer.setData("application/reactflow", JSON.stringify(node));
    event.dataTransfer.effectAllowed = "move";
  };

  const [allNodes, setAllNodes] = useState(getAvailableNodes());

  return (
    <div className={"flex flex-col h-ful"}>
      {allNodes.map((node) => {
        return (
          <div
            key={Math.random()}
            className={`flex flex-row items-center m-2 p-2 w-auto shadow-md rounded-md bg-stone-900 border-md`}
            onDragStart={(event) => onDragStart(event, node as ActionNodeType)}
            draggable
          >
            <div
              className={`p-spacing2 text-sm font-semibold text-stone-100`}
            >
              {node.name}
            </div>
          </div>
        )
      })}
    </div>
  )
}