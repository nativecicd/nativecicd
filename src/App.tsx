import React from "react";
import MainBuilder from "./builder/MainBuilder";

function App() {
  return (
    <>
      <div>
        <MainBuilder/>
      </div>
    </>
  );
}

export default App;
